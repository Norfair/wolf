module Wolf.Data.Gen
    ( subNoteIndex
    ) where

import Wolf.Data.Entry.Types.Gen ()
import Wolf.Data.Export.Types.Gen ()
import Wolf.Data.Index.Types.Gen ()
import Wolf.Data.Init.Types.Gen ()
import Wolf.Data.Note.Types.Gen ()
import Wolf.Data.NoteIndex.Types.Gen
import Wolf.Data.People.Types.Gen ()
import Wolf.Data.Suggestion.Types.Gen ()
import Wolf.Data.Types.Gen ()
