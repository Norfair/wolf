module Wolf.Data.People.Types
    ( PersonUuid
    ) where

import Import

type PersonUuid = UUID Person

data Person
